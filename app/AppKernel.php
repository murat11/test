<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/1:59 PM
 *
 */

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    function registerBundles()
    {
        $bundles = array(
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Symfony\Bundle\MonologBundle\MonologBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new \TestTask\Bundles\ImageAPIBundle\ImageAPIBundle()
        );
        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new \Symfony\Bundle\TwigBundle\TwigBundle();
            $bundles[] = new \Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
        }
        return $bundles;
    }

    function registerContainerConfiguration(LoaderInterface $loader)
    {
        $configToLoad = __DIR__ . '/config/config_' . $this->getEnvironment() . '.yml';
        if (!is_readable($configToLoad)) {
            $configToLoad = __DIR__ . '/config/config.yml';
        }
        $loader->load($configToLoad);
    }
}