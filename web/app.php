<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app/AppKernel.php';

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

$env = 'dev';

$isDebug = in_array($env, ['dev', 'test']);

if ($isDebug) {
    Debug::enable();
}
$kernel = new AppKernel($env, $isDebug);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
