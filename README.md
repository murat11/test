# Test Task from Murat Erkenov

## Installation
 - git clone this repo
 - composer install
 - rename  *config/parameters.yml.dist* to *config/parameters.yml* and specify appropriate values for all parameters
 - API is accessible via *http://yourhost/yourdirforthisproject/api* url 
 - API has 2 endpoints: */api/albums* and */api/albums/{albumId}/images/{page}*
 
## Create demo data
 - if mysql user you specified in parameters can't create databases, create them manually (3 databases: dbname, dbname_dev, dbname_test) otherwise run *bin/console doctrine:database:create* for each environment you need
 - run *bin/console doctrine:schema:create* for each environment you need
 - run *bin/console image_api:populate* for each environment you need
 
## Tests
  - To run tests rename *Tests/phpunit.xml.dist* to *Tests/phpunit.xml* and specify valid value for *<server name="KERNEL_DIR" value="/absolute/path/to/symfony/app_dir"/>*
  - All tests are in *src/ImageAPIBundle/Tests* folder
 
## Other notes
  - All significant code is in *src/ImageAPIBundle* folder
