<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/5:39 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ResponseRenderer;


use Symfony\Component\HttpFoundation\Response;

interface RendererInterface
{
    /**
     * @param mixed $data
     * @return Response
     */
    function render($data);
}