<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:43 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer;


use Knp\Component\Pager\Pagination\AbstractPagination;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\RendererInterface;

class ImageListRenderer implements RendererInterface
{

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param mixed $data
     * @return Response
     */
    function render($data)
    {
        $response = new Response();
        if (isset($data['pagination'])) {
            /** @var AbstractPagination $pagination */
            $pagination = $data['pagination'];

            $totalItemCount = $pagination->getTotalItemCount();
            $itemsPerPage = $pagination->getItemNumberPerPage();
            $currentPageNumber = $pagination->getCurrentPageNumber();
            $response->headers->set('X-Total-Count', $totalItemCount);
            $response->headers->set('X-First-Item', ($currentPageNumber - 1) * $itemsPerPage);
            $response->headers->set(
                'X-Last-Item',
                min($currentPageNumber * $itemsPerPage - 1, $totalItemCount - 1)
            );

            $normalizedItems = [];
            while ($image = $pagination->current()) {
                $normalizedItems[] = $this->serializer->normalize($image);
                $pagination->next();
            }
            $response->setContent($this->serializer->encode($normalizedItems, 'json'));
        }
        return $response;
    }

    /**
     * @param Serializer $serializer
     */
    function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }
}