<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:43 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\RendererInterface;

class AlbumListRenderer implements RendererInterface
{

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param mixed $data
     * @return Response
     */
    function render($data)
    {
        $response = new Response();
        if (is_array($data)) {
            $normalizedItems = [];
            foreach ($data as $dataItem) {
                $normalizedItems[] = $this->serializer->normalize($dataItem);
            }
            $response->setContent($this->serializer->encode($normalizedItems, 'json'));
        }
        return $response;
    }

    /**
     * @param Serializer $serializer
     */
    function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }
}