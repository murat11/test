<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/21/16/12:35 AM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer;


use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel\Exception;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\RendererInterface;

class ExceptionRenderer implements RendererInterface
{
    /**
     * @var EncoderInterface
     */
    private $encoder;

    function render($data)
    {

        if ($data instanceof Exception\HttpException) {
            $errorCode = $data->getStatusCode();
            $errorMessage = $data->getMessage();
            if (404 == $errorCode && false !== stripos($errorMessage, 'No route found')) {
                $errorCode = 400;
                $errorMessage = 'Invalid URL';
            }
            if (!empty($errorMessage) && isset(HttpFoundation\Response::$statusTexts[$errorCode])) {
                $errorMessage = HttpFoundation\Response::$statusTexts[$errorCode] . ' - ' . $errorMessage;
            }
        } else {
            $errorCode = 500;
            $errorMessage = 'Server error';
        }
        if (empty($errorMessage)) {
            $errorMessage = null;
        }
        $response = new HttpFoundation\Response();
        $response->setStatusCode($errorCode, $errorMessage);
        $response->setContent(
            $this->encoder->encode(
                [
                    'error' => [
                        'code' => $errorCode,
                        'message' => $errorMessage
                    ]
                ],
                'json'
            )
        );
        return $response;
    }

    /**
     * @param EncoderInterface $encoder
     */
    function setEncoder(EncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
}