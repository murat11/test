<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/5:35 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ResponseRenderer;


class Registry
{
    /**
     * @var RendererInterface[]
     */
    private $rendererList;

    /**
     * Registry constructor.
     * @param RendererInterface[] $rendererList
     */
    function __construct(array $rendererList)
    {
        $this->rendererList = $rendererList;
    }

    /**
     * @param string $rendererName
     * @return RendererInterface
     */
    function getRenderer($rendererName)
    {
        if (!isset($this->rendererList[$rendererName])) {
            throw new Exception('Renderer ' . $rendererName . ' not found');
        }
        return $this->rendererList[$rendererName];
    }
}