<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/3:41 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection;

abstract class BaseCommand extends Command implements DependencyInjection\ContainerAwareInterface
{

    /**
     * @var DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @param DependencyInjection\ContainerInterface $container
     */
    function setContainer(DependencyInjection\ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}