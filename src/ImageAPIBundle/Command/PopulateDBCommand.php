<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/3:30 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TestTask\Bundles\ImageAPIBundle\Entity;

class PopulateDBCommand extends BaseCommand
{

    function configure()
    {
        $this->setName('image_api:populate');
        $this->addOption(
            'clear',
            'c',
            InputOption::VALUE_NONE,
            'Should database be cleared before inserts'
        );
    }

    function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        if ($input->getOption('clear')) {
            $em->createQuery("DELETE FROM ImageAPI:Image")->execute();
            $em->createQuery("DELETE FROM ImageAPI:Album")->execute();
        }

        $albumNames = ['dogs', 'cats', 'girls', 'cars', 'sports'];

        foreach ($albumNames as $k => $albumName) {
            $theAlbum = Entity\Album::createInstance(
                [
                    'name' => ucfirst($albumName)
                ]
            );

            $numberOfImages = 5;
            if ($k > 0) {
                $numberOfImages = mt_rand(20, 40);
            }
            for ($i = 0; $i < $numberOfImages; $i++) {
                $imageOne = Entity\Image::createInstance(
                    [
                        'name' => 'Image #' . $i,
                        'url' => 'https://unsplash.it/200/300?image=' . ($k * 100 + $i)
                    ]
                );
                $imageOne->setAlbum($theAlbum);
                $em->persist($imageOne);
            }
        }
        $em->flush();
    }
}