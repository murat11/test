<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/21/16/1:26 AM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\functional;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ORM\PrepareFixturesTrait;

class APITest extends WebTestCase
{
    use  PrepareFixturesTrait;

    function testAlbums()
    {
        $client = $this->createClient();
        /** @var Router $router */
        $router = $client->getContainer()->get('router');
        $url = $router->generate('album_list');
        $client->request('GET', $url);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $json = json_decode($response->getContent(), true);
        $this->assertTrue(is_array($json));
        $this->assertCount(5, $json);
    }

    function testImages()
    {
        $client = $this->createClient();
        /** @var Router $router */
        $router = $client->getContainer()->get('router');
        $url = $router->generate(
            'image_list',
            [
                'albumId' => 2,
                'page' => 2
            ]
        );
        $client->request('GET', $url);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $json = json_decode($response->getContent(), true);
        $this->assertTrue(is_array($json));
        $this->assertTrue($response->headers->has('X-First-Item'));
        $this->assertTrue($response->headers->has('X-Last-Item'));
        $this->assertTrue($response->headers->has('X-Total-Count'));
        $this->assertEquals(10, $response->headers->get('X-First-Item'));
        $this->assertEquals(19, $response->headers->get('X-Last-Item'));
        $this->assertGreaterThan(19, $response->headers->get('X-Total-Count'));
    }

    function testError()
    {
        $client = $this->createClient();
        /** @var Router $router */
        $router = $client->getContainer()->get('router');

        $url = $router->generate('album_list');
        $url = substr($url, 0, 7);
        $client->request('GET', $url);
        $response = $client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $json = json_decode($response->getContent(), true);
        $this->assertTrue(is_array($json));
        $this->assertArrayHasKey('error', $json);
    }

}