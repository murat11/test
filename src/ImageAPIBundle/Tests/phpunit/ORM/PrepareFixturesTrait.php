<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/21/16/9:55 AM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ORM;


use Doctrine\DBAL\Connection;

trait PrepareFixturesTrait
{
    /**
     * @param Connection $connection
     */
    private function prepareFixtures(Connection $connection)
    {

        $connection->executeQuery('DELETE FROM images');
        $connection->executeQuery('DELETE FROM albums');

        $albumNames = ['dogs', 'cats', 'girls', 'cars', 'sports'];
        foreach ($albumNames as $k => $albumName) {
            $albumId = $k + 1;
            $connection->insert(
                'albums',
                [
                    'id' => $albumId,
                    'name' => $albumName
                ]
            );
            $numberOfImages = 5;
            if ($k > 0) {
                $numberOfImages = mt_rand(20, 40);
            }
            for ($i = 1; $i <= $numberOfImages; $i++) {
                $imageNumber = $k * 100 + $i;
                $connection->insert(
                    'images',
                    [
                        'id_album' => $albumId,
                        'name' => 'Image #' . $imageNumber,
                        'url' => 'https://unsplash.it/200/300?image=' . $imageNumber
                    ]
                );
            }
        }
    }

}