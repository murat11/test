<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/10:41 AM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\ORM;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TestTask\Bundles\ImageAPIBundle\Entity\Album;
use TestTask\Bundles\ImageAPIBundle\ORM\Repository\AlbumRepository;
use TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ORM\PrepareFixturesTrait;

class AlbumRepositoryTest extends KernelTestCase
{
    use PrepareFixturesTrait;

    function setUp()
    {
        self::bootKernel();
        /** @var EntityManager $em */
        $em = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $connection = $em->getConnection();
        $this->prepareFixtures($connection);
    }

    function testFetchAlbumsWithImages()
    {
        $totalAlbumsExpected = 5;
        $maxImagesInAlbumExpected = 10;

        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        /** @var EntityManager $em */
        $em = $container->get('doctrine.orm.default_entity_manager');

        /** @var AlbumRepository $repo */
        $repo = $em->getRepository('ImageAPI:Album');
        $albums = $repo->loadAlbumsWithImages(10);
        $this->assertEquals($totalAlbumsExpected, sizeof($albums), 'Total Albums number is ' . $totalAlbumsExpected);

        /** @var Album $album */
        foreach ($albums as $album) {
            $this->assertLessThan(
                $maxImagesInAlbumExpected + 1,
                sizeof($album->getImages()),
                'Images in next albums expected ' . $maxImagesInAlbumExpected
            );
        }
    }
}
