<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:30 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ResponseRenderer;


use TestTask\Bundles\ImageAPIBundle\ResponseRenderer;

class RegistryTest extends \PHPUnit_Framework_TestCase
{
    function testNotFound()
    {
        $this->expectException(ResponseRenderer\Exception::class);
        $registry = new ResponseRenderer\Registry([]);
        $registry->getRenderer('foo');
    }

    function testOk()
    {
        $registry = new ResponseRenderer\Registry(
            [
                'foo' => $this->createMock(ResponseRenderer\RendererInterface::class)
            ]
        );
        $renderer = $registry->getRenderer('foo');
        $this->assertInstanceOf(ResponseRenderer\RendererInterface::class, $renderer);
    }

}
