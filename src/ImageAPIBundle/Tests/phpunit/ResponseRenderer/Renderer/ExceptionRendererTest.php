<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:49 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ResponseRenderer\Renderer;


use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer\ExceptionRenderer;

class ExceptionRendererTest extends \PHPUnit_Framework_TestCase
{

    function testHttpException()
    {
        $contentExpected = [
            'error' => [
                'code' => 404,
                'message' => HttpFoundation\Response::$statusTexts[404] . ' - Item 345 not found'
            ]
        ];

        $renderer = new ExceptionRenderer();
        $renderer->setEncoder(new JsonEncoder());
        $response = $renderer->render(
            new HttpException(404, 'Item 345 not found')
        );
        $responseContent = $response->getContent();
        $this->assertJson($responseContent);
        $this->assertJsonStringEqualsJsonString($responseContent, json_encode($contentExpected));
        $this->assertEquals(404, $response->getStatusCode());
    }

    function test500()
    {
        $contentExpected = [
            'error' => [
                'code' => 500,
                'message' => 'Server error'
            ]
        ];

        $renderer = new ExceptionRenderer();
        $renderer->setEncoder(new JsonEncoder());
        $response = $renderer->render(
            new \LogicException('Some error')
        );
        $responseContent = $response->getContent();
        $this->assertJson($responseContent);
        $this->assertJsonStringEqualsJsonString($responseContent, json_encode($contentExpected));
        $this->assertEquals(500, $response->getStatusCode());
    }
}
