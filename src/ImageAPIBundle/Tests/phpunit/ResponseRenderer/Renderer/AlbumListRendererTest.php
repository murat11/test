<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:49 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ResponseRenderer\Renderer;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use TestTask\Bundles\ImageAPIBundle\Entity\Album;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer\AlbumListRenderer;
use TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer\AlbumNormalizer;

class AlbumListRendererTest extends \PHPUnit_Framework_TestCase
{
    function testResponseType()
    {
        $renderer = new AlbumListRenderer();
        $this->assertInstanceOf(Response::class, $renderer->render(null));
    }

    function testResponseContent()
    {
        $contentExpected = [
            [
                'id' => 1,
                'name' => 'Album #1',
                'images' => []
            ], [
                'id' => 2,
                'name' => 'Album #2',
                'images' => []
            ]
        ];

        $encoderMock = $this->createMock(JsonEncoder::class);
        $encoderMock->method('encode')->willReturnCallback(function ($data) {
            return json_encode($data);
        });
        $encoderMock->method('supportsEncoding')->willReturn(true);

        $serializer = new Serializer([new AlbumNormalizer()], [$encoderMock]);

        $renderer = new AlbumListRenderer();
        $renderer->setSerializer($serializer);
        $data = [
            Album::createInstance(
                [
                    'id' => 1,
                    'name' => 'Album #1'
                ]
            ),
            Album::createInstance(
                [
                    'id' => 2,
                    'name' => 'Album #2'
                ]
            )
        ];
        $response = $renderer->render(
            $data
        );
        $responseContent = $response->getContent();
        $this->assertJson($responseContent);
        $this->assertJsonStringEqualsJsonString($responseContent, json_encode($contentExpected));
    }
}
