<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/6:49 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\ResponseRenderer\Renderer;


use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use TestTask\Bundles\ImageAPIBundle\Entity\Image;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\Renderer\ImageListRenderer;
use TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer\ImageNormalizer;

class ImageListRendererTest extends \PHPUnit_Framework_TestCase
{

    function testResponseContent()
    {
        $contentExpected = [
            [
                'id' => 1,
                'name' => 'Image #1',
                'url' => 'image01.png'
            ], [
                'id' => 2,
                'name' => 'Image #2',
                'url' => 'image02.png'
            ]
        ];

        $encoderMock = $this->createMock(JsonEncoder::class);
        $encoderMock->method('encode')->willReturnCallback(function ($data) {
            return json_encode($data);
        });
        $encoderMock->method('supportsEncoding')->willReturn(true);

        $serializer = new Serializer([new ImageNormalizer()], [$encoderMock]);
        $renderer = new ImageListRenderer();
        $renderer->setSerializer($serializer);

        $pagination = new SlidingPagination([]);
        $pagination->setItems(
            [
                Image::createInstance(
                    [
                        'id' => 1,
                        'name' => 'Image #1',
                        'url' => 'image01.png'
                    ]
                ),
                Image::createInstance(
                    [
                        'id' => 2,
                        'name' => 'Image #2',
                        'url' => 'image02.png'
                    ]
                )

            ]
        );
        $pagination->setCurrentPageNumber(1);
        $pagination->setTotalItemCount(2);
        $pagination->setItemNumberPerPage(10);
        $response = $renderer->render(
            [
                'pagination' => $pagination
            ]
        );
        $responseContent = $response->getContent();
        $this->assertEquals(2, $response->headers->get('X-Total-Count'));
        $this->assertEquals(0, $response->headers->get('X-First-Item'));
        $this->assertEquals(1, $response->headers->get('X-Last-Item'));
        $this->assertJson($responseContent);
        $this->assertJsonStringEqualsJsonString($responseContent, json_encode($contentExpected));
    }
}
