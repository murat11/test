<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/10:24 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\Serializer\Normalizer;


use TestTask\Bundles\ImageAPIBundle\Entity\Image;
use TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer\ImageNormalizer;

class ImageNormalizerTest extends \PHPUnit_Framework_TestCase
{
    function testSupportsNormalization()
    {
        $albumNormalizer = new ImageNormalizer();
        $this->assertTrue(
            $albumNormalizer->supportsNormalization(new Image())
        );
        $this->assertFalse(
            $albumNormalizer->supportsNormalization(null)
        );
    }

    function testNormalizedWell()
    {
        $imageExpected = [
            'id' => 148,
            'name' => 'Test Image',
            'url' => '/img0.png'
        ];

        $imageObject = new Image();
        $imageObject->setId($imageExpected['id']);
        $imageObject->setName($imageExpected['name']);
        $imageObject->setUrl($imageExpected['url']);

        $imageNormalizer = new ImageNormalizer();
        $normalizedImage = $imageNormalizer->normalize($imageObject);

        $this->assertEquals($imageExpected, $normalizedImage);
    }
}
