<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/8:54 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\Serializer\Normalizer;


use Symfony\Component\Serializer\Serializer;
use TestTask\Bundles\ImageAPIBundle\Entity\Album;
use TestTask\Bundles\ImageAPIBundle\Entity\Image;
use TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer\AlbumNormalizer;

class AlbumNormalizerTest extends \PHPUnit_Framework_TestCase
{

    function testSupportsNormalization()
    {
        $albumNormalizer = new AlbumNormalizer();
        $this->assertTrue(
            $albumNormalizer->supportsNormalization(new Album())
        );
        $this->assertFalse(
            $albumNormalizer->supportsNormalization(null)
        );
    }

    function testNormalizedWell()
    {

        $serializerMock = $this->createMock(Serializer::class);
        $serializerMock->method('normalize')->willReturn(null);

        $albumExpected = [
            'id' => 148,
            'name' => 'Test Album',
            'images' => [
                null
            ]
        ];

        $albumObject = new Album();
        $albumObject->setId($albumExpected['id']);
        $albumObject->setName($albumExpected['name']);
        $albumObject->setImages(
            [
                new Image()
            ]
        );

        $albumNormalizer = new AlbumNormalizer();
        $albumNormalizer->setSerializer($serializerMock);
        $normalizedAlbum = $albumNormalizer->normalize($albumObject);

        $this->assertEquals($albumExpected, $normalizedAlbum);
    }
}
