<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/11:49 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Tests\phpunit\Controller;


use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer;

class APIControllerTest extends \PHPUnit_Framework_TestCase
{

    function testRenderResponse()
    {

        $container = new Container();
        $rendererMock = $this->createMock(ResponseRenderer\RendererInterface::class);
        $rendererMock->method('render')->willReturn(
            new Response('Hello world!')
        );
        $container->set(
            'image_api.response_renderer',
            new ResponseRenderer\Registry(
                [
                    'hello_renderer' => $rendererMock
                ]
            )
        );

        $controller = new APIController();
        $controller->setContainer($container);
        $response = $controller->renderResponse(null, 'hello_renderer');
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals('Hello world!', $response->getContent());
    }
}
