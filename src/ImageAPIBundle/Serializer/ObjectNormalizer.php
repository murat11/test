<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 7/23/16/11:26 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class ObjectNormalizer implements NormalizerInterface, SerializerAwareInterface
{

    /**
     * @var SerializerNormalizerInterface
     */
    protected $serializer;

    /**
     * Sets the serializer.
     *
     * @param SerializerInterface $serializer
     */
    function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
}