<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/8:01 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer;


use TestTask\Bundles\ImageAPIBundle\Entity;
use TestTask\Bundles\ImageAPIBundle\Serializer\ObjectNormalizer;

class ImageNormalizer extends ObjectNormalizer
{
    /**
     * @param Entity\Image $object
     * @param null $format
     * @return bool
     */
    function supportsNormalization($object, $format = null)
    {
        return $object instanceof Entity\Image;
    }

    /**
     * @param Entity\Image $object
     * @param null $format
     * @param array $context
     * @return array
     */
    function normalize($object, $format = null, array $context = array())
    {
        $normalized = [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'url' => $object->getUrl()
        ];
        return $normalized;

    }
}