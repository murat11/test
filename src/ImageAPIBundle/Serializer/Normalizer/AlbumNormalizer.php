<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/8:01 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Serializer\Normalizer;


use TestTask\Bundles\ImageAPIBundle\Entity;
use TestTask\Bundles\ImageAPIBundle\Serializer\ObjectNormalizer;

class AlbumNormalizer extends ObjectNormalizer
{
    /**
     * @param Entity\Album $object
     * @param null $format
     * @return bool
     */
    function supportsNormalization($object, $format = null)
    {
        return $object instanceof Entity\Album;
    }

    /**
     * @param Entity\Album $object
     * @param null $format
     * @param array $context
     * @return array
     */
    function normalize($object, $format = null, array $context = array())
    {
        $normalized = [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'images' => []
        ];

        $images = $object->getImages();
        if (is_array($images) && sizeof($images)) {
            foreach ($images as $image) {
                $normalized['images'][] = $this->serializer->normalize($image);
            }
        }
        return $normalized;
    }
}