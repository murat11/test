<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 7/22/16/12:41 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Serializer;


use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

interface SerializerNormalizerInterface extends SerializerInterface, NormalizerInterface
{
}