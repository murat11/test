<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/2:02 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Controller;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer;

abstract class APIController extends Controller
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (!isset($this->entityManager)) {
            if (!$this->container->has('doctrine.orm.default_entity_manager')) {
                throw new \LogicException('Can not initialize Entity Manager');
            }
            $this->entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        }
        return $this->entityManager;
    }

    /**
     * @param mixed $data
     * @param string $rendererName
     * @return Response
     */
    protected function renderResponse($data, $rendererName)
    {
        /** @var ResponseRenderer\Registry $responseRendererManager */
        $responseRendererManager = $this->container->get('image_api.response_renderer');
        $responseRenderer = $responseRendererManager->getRenderer($rendererName);
        /** @var Response $response */
        $response = $responseRenderer->render($data);
        return $response;
    }
}