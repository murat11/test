<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/2:10 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Controller\API;


use Knp\Component\Pager\Pagination\AbstractPagination;
use Knp\Component\Pager\Paginator;
use TestTask\Bundles\ImageAPIBundle\Controller\APIController;

class ImageController extends APIController
{

    function getListAction($albumId, $page = 1)
    {
        $itemsPerPage = 10;

        $qb = $this->getEntityManager()
            ->getRepository('ImageAPI:Image')
            ->createQueryBuilder('image');
        $qb->where('image.album =?1')->setParameter(1, $albumId);
        $qb->setMaxResults(10);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');

        /** @var AbstractPagination $pagination */
        $pagination = $paginator->paginate(
            $qb,
            $page,
            $itemsPerPage
        );
        return $this->renderResponse(
            [
                'pagination' => $pagination
            ],
            'image_list'
        );
    }
}