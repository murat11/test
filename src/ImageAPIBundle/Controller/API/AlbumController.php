<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/2:10 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Controller\API;


use TestTask\Bundles\ImageAPIBundle\Controller\APIController;
use TestTask\Bundles\ImageAPIBundle\ORM\Repository\AlbumRepository;

class AlbumController extends APIController
{

    function getListAction()
    {
        /** @var AlbumRepository $repo */
        $repo = $this->getEntityManager()->getRepository('ImageAPI:Album');
        $albums = $repo->loadAlbumsWithImages();

        return $this->renderResponse(
            $albums,
            'album_list'
        );
    }
}