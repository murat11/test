<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/20/16/3:41 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\EventDispatcher\Controller;


use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel\Event;

class DeveloperBarEnablingListener
{
    /**
     * @var string
     */
    private $environment;

    function onKernelResponse(Event\FilterResponseEvent $event)
    {

        if (!$event->isMasterRequest()) {
            return null;
        }
        if (!in_array($this->environment, ['dev'])) {
            return null;
        }

        /** @var HttpFoundation\Request $request */
        $request = $event->getRequest();

        /** @var HttpFoundation\Response $response */
        $response = $event->getResponse();
        switch ($request->getRequestFormat()) {
            case 'json':
                $prettyPrintLang = 'js';
                $content = $response->getContent();
                break;

            case 'xml':
                $prettyPrintLang = 'xml';
                $content = $response->getContent();
                break;

            default:
                return;
        }

        $response->setContent('
<html>
    <body>
<pre class="prettyprint lang-' . $prettyPrintLang . '">' . $content . '</pre>
    </body>
</html>
');

        $response->headers->set('Content-Type', 'text/html; charset=UTF-8');
        $request->setRequestFormat('html');
        $event->setResponse($response);

    }

    /**
     * @param string $environment
     */
    function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

}