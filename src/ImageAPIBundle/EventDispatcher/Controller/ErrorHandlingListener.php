<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/19/16/5:54 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\EventDispatcher\Controller;


use Monolog\Logger;
use Symfony\Component\HttpKernel\Event;
use Symfony\Component\HttpKernel\Exception;
use TestTask\Bundles\ImageAPIBundle\ResponseRenderer\RendererInterface;


class ErrorHandlingListener
{
    /**
     * @var RendererInterface
     */
    private $responseRenderer;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var Logger
     */
    private $logger;

    function onKernelException(Event\GetResponseForExceptionEvent $event)
    {

        if (in_array($this->environment, ['dev'])) {
            return null;
        }
        $pathInfo = $event->getRequest()->getPathInfo();
        if ('/api' != $pathInfo && 0 !== strpos($pathInfo, '/api/')) {
            return null;
        }
        $x = $event->getException();
        if (!($x instanceof Exception\HttpException)) {
            $this->logger->addCritical($x->getMessage());
        }
        $response = $this->responseRenderer->render($x);
        $event->setResponse($response);
    }

    /**
     * @param RendererInterface $responseRenderer
     */
    function setResponseRenderer(RendererInterface $responseRenderer)
    {
        $this->responseRenderer = $responseRenderer;
    }

    /**
     * @param string $environment
     */
    function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param Logger $logger
     */
    function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }
}