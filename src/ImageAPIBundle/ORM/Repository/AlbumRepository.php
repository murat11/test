<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/9:50 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\ORM\Repository;


use Doctrine\ORM\EntityRepository;
use TestTask\Bundles\ImageAPIBundle\Entity\Album;
use TestTask\Bundles\ImageAPIBundle\Entity\Image;

class AlbumRepository extends EntityRepository
{

    function loadAlbumsWithImages($imagesPerAlbum = 10)
    {
        /** @var Album[] $albums */
        $albums = [];
        $connection = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT a.name AS album_name, i.id AS id_image, i.name AS image_name, i.* 
              FROM albums a 
         LEFT JOIN  images i ON (a.id =i.id_album) 
         LEFT JOIN images ii ON (i.id_album=ii.id_album AND i.id>=ii.id) 
          GROUP BY i.id  
    HAVING COUNT(*) <= ?';

        $result = $connection->executeQuery($sql, [$imagesPerAlbum]);

        while ($row = $result->fetch()) {
            $albumId = $row['id_album'];
            if (!isset($albums[$albumId])) {
                $albums[$albumId] = Album::createInstance(
                    [
                        'id' => $albumId,
                        'name' => $row['album_name']
                    ]
                );
            }
            /** @var Album $album */
            $album = &$albums[$albumId];

            /** @var Image $image */
            $image = Image::createInstance(
                [
                    'id' => $row['id_image'],
                    'name' => $row['image_name'],
                    'url' => $row['url'],
                    'album' => $album
                ]
            );
            $album->addImage($image);

        }
        return $albums;
    }
}