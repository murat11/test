<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/5:00 PM
 *
 */
namespace TestTask\Bundles\ImageAPIBundle\Entity;

class Album extends BaseEntity
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var String
     */
    private $name;

    /**
     * @var Image[]
     */
    private $images;

    /**
     * @return integer
     */
    function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Image[]
     */
    function getImages()
    {
        return $this->images;
    }

    /**
     * @param Image[] $images
     */
    function setImages(array $images)
    {
        $this->images = $images;
    }

    /**
     * @param Image $image
     */
    function addImage(Image $image)
    {
        $this->images[$image->getId()] = $image;
    }
}