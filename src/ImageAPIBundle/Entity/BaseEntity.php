<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/5:32 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Entity;


abstract class BaseEntity
{

    /**
     * Creates Entity.
     * If $data is passed with key-value pairs, it tries to assign appropriate fields using setter methods
     *
     * @param array|null $data
     * @return BaseEntity
     */
    static function createInstance(array $data = null)
    {
        $entity = new static();

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $methodName = 'set' . ucfirst($key);
                if (method_exists($entity, $methodName)) {
                    $entity->$methodName($value);
                }
            }
        }
        return $entity;
    }
}