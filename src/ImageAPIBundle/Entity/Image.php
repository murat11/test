<?php
/**
 * Author: Murat Erkenov
 * Date/Time: 10/18/16/5:00 PM
 *
 */

namespace TestTask\Bundles\ImageAPIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Image extends BaseEntity
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var String
     */
    private $name;


    /**
     * @var String
     */
    private $url;

    /**
     * @var Album
     */
    private $album;

    /**
     * @return integer
     */
    function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Album
     */
    function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param Album $album
     */
    function setAlbum(Album $album = null)
    {
        $this->album = $album;
    }

    /**
     * @return String
     */
    function getUrl()
    {
        return $this->url;
    }

    /**
     * @param String $url
     */
    function setUrl($url)
    {
        $this->url = $url;
    }
}